package org.springframework.samples.petclinic;

import java.io.IOException;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;

import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.secretsmanager.SecretsManagerClient;
import software.amazon.awssdk.services.secretsmanager.model.GetSecretValueRequest;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SecretsManagerEnvironmentPostProcessor implements EnvironmentPostProcessor {

	private static final String SECRET_NAME = "finalTestDbV1"; // Ensure this is
																// correctly set in
																// your AWS Secrets
																// Manager

	@Override
	public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
		SecretsManagerClient client = SecretsManagerClient.builder().region(Region.of("us-west-2")) // Specify
																									// the
																									// AWS
																									// region
				.build();

		GetSecretValueRequest valueRequest = GetSecretValueRequest.builder().secretId(SECRET_NAME).build();

		String secretValue = client.getSecretValue(valueRequest).secretString();
		System.out.println("-----------------------------------------");
		try {
			// Parse the secret value which is in JSON format
			Map<String, Object> secretMap = new ObjectMapper().readValue(secretValue, Map.class);
			// Add the secrets to the Spring environment
			System.out.println("Secret Value: " + secretValue);
			environment.getPropertySources().addFirst(new MapPropertySource("awsSecrets", secretMap));
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to parse secrets from AWS Secrets Manager: " + e.getMessage(), e);
		}
	}

}